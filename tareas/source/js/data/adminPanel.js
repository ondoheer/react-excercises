var Accordeon = require("./../components/accordeon/accordeon"),
	Form = require("./../components/form/form"),
	Contacts = require("./../components/contacts/contactsApp"),
	Lifecycle = require("./../components/lifecycle/lifecycle");

var components = [
	{
		name: "Accordeon",
		component: Accordeon 
	},
	{
		name: "Form",
		component: Form
	},
	{
		name: "Contacts Filter",
		component: Contacts
	},
	{
		name: "Estados",
		component: Lifecycle
	}
]

module.exports = components