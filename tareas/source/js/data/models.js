var months_data = [
{
	name: "Enero",
	expenses: [
		{
			name: "chifa",
			category: "comer fuera",
			amount: 23.4
		},
		{
			name: "bembos",
			category: "comer fuera",
			amount: 13.45
		},
		{
			name: "uber",
			category: "taxi",
			amount: 6.4
		},
		{
			name: "polos",
			category: "ropa",
			amount: 53.0
		}
	]
},
{
	name: "Febrero",
	expenses: [
		{
			name: "Tamales",
			category: "comer fuera",
			amount: 13.23
		},
		{
			name: "Rock the kasbah",
			category: "cine",
			amount: 24.0
		},
		{
			name: "uber",
			category: "taxi",
			amount: 6.45
		},
		{
			name: "jeans",
			category: "ropa",
			amount: 83.0
		}
	]
}


]

module.exports = months_data