var React = require("react");
var ReactDOM = require("react-dom");


var Lifecycle = React.createClass({
	getInitialState: function(){
		// console.log("Get initial state called");
		return {state1: "Where's Daddy?"}
	},
	getDefaultProps: function(){
		// console.log("Get default props called");
		return {name: {"value":"I know you want it"}}
	},
	componentWillMount: function(){
		// console.log("Compnent will mount called", arguments);
	},
	componentDidMount: function(){
		// console.log("ComponentDidMount called", arguments);
		var self = this;
		var counter = 1;
		var update = function(){
			var toUpdate = self.state.state1 == "Here's Daddy!" ? "Where's Daddy?" : "Here's Daddy!";
			
			setTimeout(function(){
				self.props.name.value = self.props.name.value == "You know you want it" ? "I know you want it": "You know you want it"; // esto no funciona
				self.setState({state1: toUpdate});
				self.forceUpdate();	// suena feo esto de force update
				self.refs.counter.innerText = counter;
				counter++;		
				global.requestAnimationFrame(update);
			}, 1500)

		}		
		global.requestAnimationFrame(update)		
	},
	shouldComponentUpdate: function(){
		// console.log("should shouldComponentUpdate", arguments);
		return true
	},
	componentWillUpdate: function(){
		// console.log("component will update", arguments);
	},
	componentDidUpdate: function(){
		// console.log("componentDidUpdate", arguments);
	},
	componentWillReceiveProps: function(){
		// console.log("RECEIVED PROPS", arguments);
	},
	onCopyHandler: function(e){
		
		e.clipboardData.setData('text/plain', 'Hola, Soy tu computadora, deja de copiar tonterias!');
	    e.clipboardData.setData('text/html', '<b>Hola, Soy tu computadora, deja de copiar tonterias!</b>');
	    e.preventDefault();
	},
	render: function(){
		return (
			<nav>
				<span onCopy={this.onCopyHandler}>{this.state.state1}</span> -
				<span>{this.props.name.value}</span>
				<div ref="counter"> Seré borrado </div>
			</nav>
		)
	}
});

Lifecycle.proptypes = {

}
module.exports = Lifecycle