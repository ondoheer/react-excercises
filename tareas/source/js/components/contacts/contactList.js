var React = require("react");
var ContactItem = require("./contactItem");


var ContactList = React.createClass({
	render: function(){
		// TODO revisar esto
		var filterText = this.props.filterText
		var filteredContacts = this.props.contacts.filter(
									function(contact){
										return contact.name.indexOf(filterText) !== -1
									})
		return (
			<ul>
				{filteredContacts.map(function(contact){
					return <ContactItem 
								key={contact.email}
								name={contact.name}
								email={contact.email} /> 
					})
				}
			</ul>
		)
	}
});

ContactList.proptypes = {
	empployee: React.PropTypes.arrayOf(React.PropTypes.object)
}

module.exports = ContactList