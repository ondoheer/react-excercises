var React = require("react");
var SearchBar = require("./searchBar"),
	ContactList = require("./contactList"),
	
	employees = require("./../../data/employees");

var ContactsApp = React.createClass({
	getInitialState: function(){
		return {filterText: ""}
	},
	handleUserInput: function handleUserInput(searchTerm){
		this.setState({filterText: searchTerm})
	},
	render: function(){
		return (
			<div>
				<SearchBar filterText={this.state.filterText}
							onUserInput={this.handleUserInput} />
				<ContactList contacts={employees} filterText={this.state.filterText}/>
			</div>
		)
	}
});


ContactsApp.proptypes = {
	contacts: React.PropTypes.arrayOf(React.PropTypes.object),
}

module.exports = ContactsApp