var React = require("react");


var SearchBar = React.createClass({
	handleChange: function handleChange(e){
		this.props.onUserInput(e.target.value)
	},
	render: function(){

		return (
			<input type="search" 
					placeholder="buscar empleado" 
					value={this.props.filterText}
					onChange={this.handleChange}/>
		)
	}
});


SearchBar.propTypes = {
	onUserInput: React.PropTypes.func.isRequired,
    filterText: React.PropTypes.string.isRequired
}

module.exports = SearchBar