var React = require("react");

var ContactItem = React.createClass({
	render: function(){
		return (
			<li>{this.props.name} - <a href={"mailto:" + this.props.email}>{this.props.email}</a></li>
		)
	}
});

ContactItem.propTypes = {
	name: React.PropTypes.string.isRequired,
	email: React.PropTypes.string.isRequired
}


module.exports = ContactItem