var React = require("react");
var AdminControl = require("./adminControl");
var AdminItem = require("./adminItem");

var panelComponents = require("./../data/adminPanel");



var AdminPanel = React.createClass({
	getInitialState: function(){
		return {			
			selectedIndex: 0
		}
	},
	handleTabClick: function handleTabClick(index){
		this.setState({selectedIndex: index})
	},
	render: function(){
		var self = this
		return (
			<div id="adminPanel" className="admin">
				<ul className="admin__controls">
					{
						panelComponents.map(function(component, index){
							return <AdminControl  clickHandler={self.handleTabClick.bind(self, index )} key={"admin-control" + index} id={"control"+index}  controlName={component.name} />
						})
					}						
				</ul>
				<div className="panels">
					{
						panelComponents.map(function(component, index){
							
							return <AdminItem 
									className={self.state.selectedIndex === index ? "admin__panel": "admin__panel hide"}
								 	key={component.name + "-panel"} controlId={index} id="accordeon-panel"  component={component.component}/>
						})
					}
					
				</div>

			</div>
		)
	}
});


module.exports = AdminPanel