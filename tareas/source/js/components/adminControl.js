var React = require("react");


var AdminControl = React.createClass({
	
	render: function(){
		return (
			
				<li 

					id={this.props.id}
					className="admin__control"
					onClick={this.props.clickHandler}
					data-selected={this.props.selected}
					>
				{this.props.controlName}
					
				</li>
				
		)
	}
});

module.exports = AdminControl