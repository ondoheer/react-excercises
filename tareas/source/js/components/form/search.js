var React = require("react");

var SearchForm = React.createClass({
	getInitialState: function(){
		return {
			searchTerm: "buscar"
		}
	},
	handleChange: function(e){
		this.setState({
			searchTerm: e.target.value
		})
	},
	render: function(){
		return (
			<div>
				Search Term: <input type="search" onChange={this.handleChange} value={this.state.searchTerm} /> 
			</div>
		)
	}
});

module.exports = SearchForm