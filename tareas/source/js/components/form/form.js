var React = require("react");
var SearchComponent = require("./search");
var FormComponent = React.createClass({

	componentDidMount: function(){
		//rome(document.getElementById("calendar")); 
		rome(this.refs.calendar); 
	},
	render: function(){
		return (
			<div>
				<SearchComponent />
				<br/>	
			 	<label>Elige tu fecha: </label>
				<input type="text" className="calendar" id="calendar" ref="calendar" />
			</div>
		)
	}
});

module.exports = FormComponent