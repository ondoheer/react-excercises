var React = require("react");


var AdminItem = React.createClass({
	render: function(){
		var Component = this.props.component;
		return (
			<section data-parent={this.props.controlId}   className={this.props.className}>
				<Component />
			</section>
		)
	}
});

module.exports = AdminItem