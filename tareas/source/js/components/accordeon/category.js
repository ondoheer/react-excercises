var React = require("react");



var CategoryComponent = React.createClass({
	render: function(){
		
		return (
			<li>
				<span className="category">{this.props.name}</span> 
				<span className="amount">S/. {this.props.monthlySum}</span>
			</li>
		)
	}
});

module.exports = CategoryComponent