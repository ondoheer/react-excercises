var React = require("react");
var MonthComponent = require("./month"),
	months_data = require("./../../data/models");

var Accordeon = React.createClass({
	render: function(){
		return(
			<div>
				<h1>Gastos por mes</h1>
				{
					months_data.map(function(month, i){
						return <MonthComponent key={i} name={month.name} expenses={month.expenses} /> 
					})
				}
			</div>
		)
	}
});


module.exports = Accordeon;