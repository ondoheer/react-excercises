var React = require("react"),
	CategoryComponent = require("./category");


var MonthComponent = React.createClass({
	getInitialState: function(){
		return {
			show: false,
			_class: ""
		}
	},
	toggleArea: function(){
		this.setState({show: !this.state.show})
		
	},
	totalExpenses: function(expenses){
		var exps = [];
		for (expense of expenses){
			exps.push(expense.amount)
		}

		return exps.reduce(function(a,b){
			return a+b
		})
	
	},
	render: function(){
		var categoryDetails;
		if (this.state.show){
			categoryDetails = (
				this.props.expenses.map(function(expense, i){
					return <CategoryComponent key={i} name={expense.category} monthlySum={expense.amount} />		
				})
			)
		}
		
		return (
			<div className="month" >
				<h2 onClick={this.toggleArea}> {this.props.name} - S/. {this.totalExpenses(this.props.expenses)}</h2>
				<ul className={this.state._class}>
					{categoryDetails}
					
				</ul>

			</div>
		)
	}
});

MonthComponent.propTypes = {
	categoryDetails: React.PropTypes.instanceOf(CategoryComponent)
}

module.exports = MonthComponent