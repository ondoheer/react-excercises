var React = require("react"),
	ReactDom = require("react-dom"),
	ProgrammmerPhrase = require("./programmer_phrase"),
	ProgrammerSelect = require("./programmer_select"),
	ProgrammersData = require("./programmer_data");



var HelloComponent = React.createClass({
	render: function(){
		var styles = {"fontSize":"45px"};
		return <div className="message" style={ styles }>Hola {this.props.name}</div>;
	}
});

ReactDom.render(<HelloComponent name="Athelas" />, document.querySelector("#app"));

var AthelasTeam = React.createClass({
	getInitialState: function(){
		return {name: "Esen"}
	},
	selectPhrase: function(e){
		this.setState({
			name: e.target.value
		})
	},
	render: function(){
		return <div className="programmer">
					<ProgrammerSelect onChange={this.selectPhrase} />
					<ProgrammmerPhrase name={this.state.name} />					
				</div>
				
	}
});



﻿

ReactDom.render(<AthelasTeam />, document.querySelector("#employees"));