var React = require("react");

var ProgrammmerPhrase = React.createClass({
	chooseName: function(){
		var phrase;
		if (this.props.name === "Martin"){
			phrase = "Tu si estas en otro level";
		} else if (this.props.name === "Esen"){
			phrase = "Esto lo hago en 5 minutos";
		} else if (this.props.name === "Renato"){
			phrase = "Mejor le hacemos un VPS, digo nomás";
		} else if (this.props.name === "Gerald"){
			phrase = "Me siento Gordaaaaa";
		} else if (this.props.name === "Heison"){
			phrase = "Y no has pensado en hacer un tema de WP y venderlo? Negociaso!";
		} else if (this.props.name === "Polo"){
			phrase = "A quién le toca lavar los platos?";
		} else {
			phrase = "Aquí nadie habla nunca";
		} 
		return phrase
	},
	render: function(){
		var phrase = this.chooseName();
		return 	<div>
					<h3>{this.props.name}:</h3>
					<p>"{phrase}"</p>
				</div>
	}
});

module.exports = ProgrammmerPhrase;