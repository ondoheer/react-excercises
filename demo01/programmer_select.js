var React = require("react");

var ProgrammerSelect = React.createClass({

	render: function(){
		return <select onChange={this.props.onChange} >
					<option value="Esen">Esen</option>
					<option value="Renato">Renato</option>
					<option value="Martin">Martin</option>
					<option value="Gerald">Gerald</option>
					<option value="Polo">Polo</option>
					<option value="Heison">Heison</option>
				</select>
	}
});

module.exports = ProgrammerSelect;