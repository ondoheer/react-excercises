var React = require("react");
var ProgrammerPhrase = require("./programmer_phrase");



var ProgrammersData = React.createClass({
	phrases: [
		{name: "Martin", phrase: "Tu si estas en otro level"},
		{name: "Esen", phrase: "Esto lo hago en 5 minutos"}
	],
	render: function(){		
		return (
			<div>
				{this.phrases.map(function(phrase){
					return <ProgrammerPhrase name={phrase.name} phrase={phrase.phrase} /> 
				})}
			</div>
			)

		
	}

});

module.exports = ProgrammersData

